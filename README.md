# DaggerConfig #

DaggerConfig is a library to allow for quick configuratin of C# programs.
It is a personal project, but anyone can contribute to it, or use it for their own purposes.

## How to use? ##

After you have cloned the repository you can either;

*  Include the project in your solution 
*  mark it as reference

Or

* Build it
* reference the resulting dll

For further reference on how to use this library, please read the [Wiki](https://bitbucket.org/CapCalamity/daggerconfig/wiki)

### Contribution guidelines ###

If you want to contribute, please try to stick with the style of the project.
Otherwise there is not a lot to consider, just send a pull request.