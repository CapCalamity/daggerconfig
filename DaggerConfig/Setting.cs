﻿using System;

namespace DaggerConfig
{
    internal class Setting
    {
        private readonly string _key;
        private readonly object _value;

        public string Key
        {
            get { return _key; }
        }

        public object Value
        {
            get { return _value; }
        }

        public Setting(string key, object value)
        {
            _key = key;
            _value = value;
        }

        public override string ToString()
        {
            return String.Format("{0}:{1}", _key, _value);
        }

        public override int GetHashCode()
        {
            return _key.GetHashCode() + _value.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is Setting)
                return Equals(obj as Setting);
            return false;
        }

        public bool Equals(Setting set)
        {
            return set != null
                   && set.Key.Equals(_key)
                   && set.Value.Equals(_value);
        }

        public static bool operator ==(Setting left, Setting right)
        {
            if (System.Object.ReferenceEquals(left, right))
                return true;

            if (((object) left == null) || ((object) right == null))
                return false;

            return left._key == right._key
                   && left._value == right._value;
        }

        public static bool operator !=(Setting left, Setting right)
        {
            return !(left == right);
        }
    }
}
