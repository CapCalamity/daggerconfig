﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using DaggerConfig.Sources;

namespace DaggerConfig
{
    public class Config
    {
        public ErrorMode ErrorHandlingMode { get; set; }
        public bool AllowOverride { get; set; }
        public bool MatchCaseSensitive { get; set; }

        private readonly Dictionary<string, List<Setting>> _cache;
        private readonly ConfigSource _source;
        private ReadMode _mode;
        private bool _sealed;

        internal Config()
        {
            ErrorHandlingMode = ErrorMode.Exception;
            AllowOverride = true;
            MatchCaseSensitive = false;
            _mode = ReadMode.LazyCache;
            _sealed = false;
            _cache = new Dictionary<string, List<Setting>>();
            _source = new MemorySource();
        }

        internal Config(Dictionary<string, Setting[]> cache) : this()
        {
            _cache = new Dictionary<string, List<Setting>>();
            foreach (var kvp in cache)
            {
                foreach (var setting in kvp.Value)
                    _cache[kvp.Key].Add(setting);
            }
        }

        internal Config(ConfigSource source) : this()
        {
            if (source == null)
                throw new ArgumentNullException();

            _source = source;
        }

        private void Initialize()
        {
            _source.Initialize();
            if (_mode == ReadMode.CacheOnly)
                CacheAllFromSource();
        }

        /// <summary>
        /// Create a new instance which stores Settings in memory
        /// </summary>
        /// <param name="mode">How to handle Caching and reads to Source</param>
        /// <returns>new Config bound to Memory</returns>
        public static Config CreateMemory(ReadMode mode)
        {
            var conf = new Config(new MemorySource()) {_mode = mode};
            conf.Initialize();
            return conf;
        }

        /// <summary>
        /// Create a new instance which stores Settings in a local Textfile.
        /// The Textfiles format is based on .ini files.
        /// </summary>
        /// <param name="path">Path to the config file</param>
        /// <param name="mode">How to handle Caching and reads to Source</param>
        /// <returns>new Config bound to the given file</returns>
        public static Config CreateTextSource(ReadMode mode, string path)
        {
            var conf = new Config(new TextfileSource(path)) {_mode = mode};
            conf.Initialize();
            return conf;
        }

        /// <summary>
        /// Read all settings from the source and cache them.
        /// </summary>
        public void CacheAllFromSource()
        {
            _cache.Clear();
            foreach (var group in _source.GetAllSettings())
                _cache.Add(group.Key, group.Value.ToList());
        }

        /// <summary>
        /// Seal the Config and perform subsequent Reads only against the Cache.
        /// </summary>
        public void Seal()
        {
            _sealed = true;
            _mode = ReadMode.CacheOnly;
        }

        /// <summary>
        /// Try to get a value 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="group"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public T Get<T>(string group, string key)
        {
            if (String.IsNullOrWhiteSpace(key) || String.IsNullOrWhiteSpace(group))
            {
                if (ErrorHandlingMode == ErrorMode.DefaultValue)
                    return default(T);
                throw new ArgumentException("group and key cannot be null or empty");
            }

            if (_mode == ReadMode.CacheOnly)
            {
                if (!GroupContainsSettings(group) || _cache[group].Any(set => set.Key == key) == false)
                {
                    if (ErrorHandlingMode == ErrorMode.DefaultValue)
                        return default(T);
                    throw new KeyNotFoundException(String.Format("setting {0}/{1} not available", group, key));
                }
            }

            Setting setting = _mode == ReadMode.PassThrough
                                  ? _source.GetSetting(@group, key)
                                  : _cache[@group].First(set => set.Key.Equals(key, MatchCaseSensitive
                                                                                        ? StringComparison.InvariantCulture
                                                                                        : StringComparison.InvariantCultureIgnoreCase));

            if (_mode == ReadMode.LazyCache)
                _cache[group].Add(setting);

            try
            {
                if (setting.Value is T)
                    return (T) setting.Value;
                return (T) Convert.ChangeType(setting.Value, typeof (T));
            }
            catch (Exception)
            {
                if (ErrorHandlingMode == ErrorMode.Exception)
                {
                    throw new Exception(String.Format("Type {0} is not of type {1}, could not cast nor convert.",
                        typeof (T),
                        setting.Value.GetType()));
                }
                return default(T);
            }
        }

        /// <summary>
        /// Check if the current cache has entries under the specified group
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        public bool CacheHasGroup(string group)
        {
            return _cache.ContainsKey(group);
        }

        /// <summary>
        /// Check whether the given group is set and has entries or not
        /// </summary>
        /// <param name="group">Name of the cached group</param>
        /// <returns>true if the cache has a group with one or more entries of the given name</returns>
        public bool GroupContainsSettings(string group)
        {
            return _cache.ContainsKey(group) && _cache[group].Count > 0;
        }

        /// <summary>
        /// Check if the current cache has a setting with the given key in the given group
        /// </summary>
        /// <param name="group"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool CacheHasKey(string group, string key)
        {
            return CacheHasGroup(group) && _cache[group].Any(set => set.Key == key);
        }

        public bool Set(string group, string key, object value)
        {
            if (_sealed)
                return false;

            try
            {
                var setting = new Setting(key, value);

                var has = CacheHasKey(group, key);

                if (!AllowOverride && has)
                    return false;

                if (_source.Set(group, key, value))
                {
                    if (!_cache.ContainsKey(group))
                        _cache.Add(group, new List<Setting>());

                    if (has)
                        _cache[group].Remove(_cache[group].First(set => set.Equals(setting)));

                    _cache[group].Add(setting);

                    return true;
                }

                return false;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return false;
            }
        }
    }
}
