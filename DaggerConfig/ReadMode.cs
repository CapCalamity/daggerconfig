﻿namespace DaggerConfig
{
    public enum ReadMode
    {
        /// <summary>
        /// Read settings from Cache. If they are not Cached, read them from their respective Source and Cache them.
        /// </summary>
        LazyCache,

        /// <summary>
        /// Reads are only performed against the Cache.
        /// </summary>
        CacheOnly,

        /// <summary>
        /// All reads are performed against the Source.
        /// </summary>
        PassThrough,
    }
}
