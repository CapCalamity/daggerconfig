﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace DaggerConfig.Sources
{
    internal class TextfileSource : ConfigSource
    {
        private readonly FileInfo _configFileInfo;
        private readonly Regex _groupRegex;
        private readonly Regex _settingRegex;

        public TextfileSource(string pathToFile)
        {
            _configFileInfo = new FileInfo(pathToFile);
            _groupRegex = new Regex(@"^[\s]*\[([\d\w]+)\]$", RegexOptions.Compiled | RegexOptions.Singleline);
            _settingRegex = new Regex(@"^[\s]*([\w\d]+)[\s]*=[\s]*([\w\d]+)$", RegexOptions.Compiled | RegexOptions.Singleline);
        }

        public override bool Initialize()
        {
            return _configFileInfo.Exists;
        }

        public override Setting GetSetting(string group, string key)
        {
            var allSettings = GetAllSettings();
            return allSettings.First(g => g.Key == group).Value.First(set => set.Key == key);
        }

        public override Dictionary<string, IEnumerable<Setting>> GetAllSettings()
        {
            if (!_configFileInfo.Exists)
                return new Dictionary<string, IEnumerable<Setting>>();

            var result = new Dictionary<string, IEnumerable<Setting>>();
            
            using (StreamReader reader = _configFileInfo.OpenText())
            {
                var currentGroup = "";
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();

                    if (line == null)
                        continue;

                    var groupMatch = _groupRegex.Match(line);
                    if (groupMatch.Success)
                    {
                        currentGroup = groupMatch.Groups[1].Value;
                        result.Add(currentGroup, new List<Setting>());
                        continue;
                    }

                    var settingsMatch = _settingRegex.Match(line);
                    if (settingsMatch.Success)
                    {
                        var key = settingsMatch.Groups[1].Value;
                        var value = settingsMatch.Groups[2].Value;

                        ((List<Setting>) result[currentGroup]).Add(new Setting(key, value));
                    }
                }
            }

            return result;
        }

        public override bool Set(string group, string key, object value)
        {
            try
            {
                var settings = GetAllSettings();
                if (!settings.ContainsKey(group))
                    settings.Add(group, new List<Setting>());

                var tmpList = settings[group].ToList();

                tmpList.RemoveAll(set => set.Key == key);
                tmpList.Add(new Setting(key, value));

                settings[group] = tmpList;

                return Save(settings);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public override bool Set(Dictionary<string, IEnumerable<Setting>> settings)
        {
            return Save(settings);
        }

        private bool Save(Dictionary<string, IEnumerable<Setting>> settings)
        {
            try
            {
                using (var writer = new StreamWriter(_configFileInfo.OpenWrite()))
                {
                    foreach (var kvp in settings)
                    {
                        writer.WriteLine("[{0}]", kvp.Key);
                        foreach (var setting in kvp.Value)
                            writer.WriteLine("{0}={1}", setting.Key, setting.Value);
                    }
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
