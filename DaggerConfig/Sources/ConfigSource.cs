﻿using System.Collections;
using System.Collections.Generic;

namespace DaggerConfig.Sources
{
    internal abstract class ConfigSource : IEnumerable<KeyValuePair<string, IEnumerable<Setting>>>
    {
        /// <summary>
        /// Method to completely initialize the ConfigSource
        /// </summary>
        /// <returns>Success of Initialization</returns>
        public abstract bool Initialize();

        /// <summary>
        /// Get the specified setting directly from the Source. 
        /// Will throw if none found.
        /// </summary>
        /// <param name="group">Group name of the setting</param>
        /// <param name="key">specified name of the setting</param>
        /// <returns>Setting or null</returns>
        public abstract Setting GetSetting(string group, string key);

        /// <summary>
        /// Get all setting directly from the source
        /// </summary>
        /// <returns>A list of settings</returns>
        public abstract Dictionary<string, IEnumerable<Setting>> GetAllSettings();

        /// <summary>
        /// Set a setting to the specified value and write it to the Source
        /// </summary>
        /// <param name="group">Settings Group name</param>
        /// <param name="key">Settings name</param>
        /// <param name="value">Value of the setting</param>
        /// <returns>Success</returns>
        public abstract bool Set(string group, string key, object value);

        /// <summary>
        /// Save groups of settings
        /// </summary>
        /// <param name="settings">Dictionary grouping lists of settings together</param>
        /// <returns>success of saving</returns>
        public abstract bool Set(Dictionary<string, IEnumerable<Setting>> settings);

        public virtual IEnumerator<KeyValuePair<string, IEnumerable<Setting>>> GetEnumerator()
        {
            return GetAllSettings().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
