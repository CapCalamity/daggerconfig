﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DaggerConfig.Sources
{
    /// <summary>
    /// Represent Memory-Only, non-persistent settings.
    /// </summary>
    internal class MemorySource : ConfigSource
    {
        private Dictionary<string, List<Setting>> _cache;

        public MemorySource()
        {
            _cache = new Dictionary<string, List<Setting>>();
        }

        public override bool Initialize()
        {
            return true;
        }

        public override Setting GetSetting(string groupName, string keyName)
        {
            return _cache.First(g => g.Key == groupName).Value
                         .First(setting => setting.Key == keyName);
        }

        public override Dictionary<string, IEnumerable<Setting>> GetAllSettings()
        {
            var result = new Dictionary<string, IEnumerable<Setting>>(_cache.Count);
            foreach (var kvp in _cache)
                result.Add(kvp.Key, kvp.Value);
            return result;
        }

        public override bool Set(string group, string key, object value)
        {
            try
            {
                if(!_cache.ContainsKey(group))
                    _cache.Add(group, new List<Setting>());
                
                var settings = _cache.First(kvp => kvp.Key == group).Value;
                
                settings.RemoveAll(set => set.Key == key);
                settings.Add(new Setting(key, value));

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public override bool Set(Dictionary<string, IEnumerable<Setting>> settings)
        {
            return settings
                .Aggregate(true,
                    (current1, kvp) => kvp.Value.
                                           Aggregate(current1,
                                               (current, setting) => current & Set(kvp.Key, setting.Key, setting.Value)));
        }
    }
}
