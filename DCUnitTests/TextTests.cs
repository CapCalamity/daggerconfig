﻿using DaggerConfig;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DCUnitTests
{
    [TestClass]
    public class TextTests
    {
        private const string ConfLocation = @"D:\config.ini";

        [TestMethod]
        public void Text_SourceNotNull()
        {
            var texConf = Config.CreateTextSource(ReadMode.LazyCache, ConfLocation);
            Assert.IsNotNull(texConf);
        }

        [TestMethod]
        public void Text_WriteSettings()
        {
            var texConf = Config.CreateTextSource(ReadMode.LazyCache, ConfLocation);
            Assert.IsNotNull(texConf);

            for (var g = 0; g < 10; ++g)
            {
                for (var k = 0; k < 100; ++k)
                    texConf.Set(g.ToString(), k.ToString(), g * k);
            }
        }

        [TestMethod]
        public void Text_ReadSettings()
        {
            var texConf = Config.CreateTextSource(ReadMode.LazyCache, ConfLocation);
            Assert.IsNotNull(texConf);

            for (var g = 0; g < 10; ++g)
            {
                for (var k = 0; k < 100; ++k)
                    Assert.IsTrue(texConf.Set(g.ToString(), k.ToString(), g * k));
            }

            for (int g = 10 - 1; g >= 0; g--)
            {
                for (int k = 100 - 1; k >= 0; k--)
                    Assert.IsTrue(texConf.Get<int>(g.ToString(), k.ToString()) == g * k);
            }
        }
    }
}
