﻿using DaggerConfig;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DCUnitTests
{
    [TestClass]
    public class MemoryTests
    {
        [TestMethod]
        public void Memory_SourceNotNull()
        {
            var memConf = Config.CreateMemory(ReadMode.LazyCache);
            Assert.IsNotNull(memConf);
        }

        [TestMethod]
        public void Memory_WriteSettings()
        {
            var memConf = Config.CreateMemory(ReadMode.LazyCache);
            Assert.IsNotNull(memConf);

            for (var g = 0; g < 10; ++g)
            {
                for (var k = 0; k < 100; ++k)
                    memConf.Set(g.ToString(), k.ToString(), g * k);
            }
        }

        [TestMethod]
        public void Memory_ReadSettings()
        {
            var memConf = Config.CreateMemory(ReadMode.LazyCache);
            Assert.IsNotNull(memConf);

            for (var g = 0; g < 10; ++g)
            {
                for (var k = 0; k < 100; ++k)
                    Assert.IsTrue(memConf.Set(g.ToString(), k.ToString(), g * k));
            }

            for (int g = 10 - 1; g >= 0; g--)
            {
                for (int k = 100 - 1; k >= 0; k--)
                    Assert.IsTrue(memConf.Get<int>(g.ToString(), k.ToString()) == g * k);
            }
        }
    }
}
